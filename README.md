![banner](https://banner.dapla.net/?utm_campaign=community-buildpacks&utm_source=github.com/daplanet/xelatex-buildpack&utm_medium=markdown)

# baseui [![DeepSource](https://static.deepsource.io/deepsource-badge-light-mini.svg)](https://deepsource.io/gh/daplanet/baseui/?ref=repository-badge) [![Build Status](https://travis-ci.org/daplanet/baseui.svg?branch=master)](https://travis-ci.org/daplanet/baseui) [![Coverage Status](https://coveralls.io/repos/github/daplanet/baseui/badge.svg?branch=master)](https://coveralls.io/github/daplanet/baseui?branch=master) [![License](https://badgen.net/github/license/daplanet/baseui)](https://github.com/daplanet/baseui/blob/master/LICENSE)
[![FOSSA Status](https://app.fossa.com/api/projects/git%2Bgithub.com%2Fdaplanet%2Fbaseui.svg?type=shield)](https://app.fossa.com/projects/git%2Bgithub.com%2Fdaplanet%2Fbaseui?ref=badge_shield)
Rapid PWA development framework. Uses VueJS, Hammerjs, Vuex, Vuerouter, Bulma/MDL, and require.js to rapidly develop full featured progressive web aplications.

API Trasport is provided by Axios and a SQL interface to data is provided by alasql (yes we have SQL query supports to REST in a frontend that's *NOT* GraphQL or dependant on the data source)

## Concept
Developing a frontend application should not mean learning new ecosystems, libraries, or extra tools to produce a product. This was the success behind jQuery, it emplowered developers to build better javascript applications. Now that most of the features from jQuery are core ES8 one should be able to translate that same experence into the future of reactive progressive apps. Thus the use of VueJS and this framework.

The end proccess is to allow individual developers to rapidly develop a full product that then can be put on any hosting system be it a CDN, docker cluster, ipfs or what ever. While still being lightwieght in choices to the developer nor heavily "opinionated".

If one needs sql there is AlaSQL to manipulate data in ram. If one needs GraphQL then defind that as your API class and use that instead of Axios

At the end of the day the tools should not be in the way to deliver a product and developers should be able to leverage existing skills that copmpliment both Unix Philosphy and 12 factor. That is the core philosophy behind [BaseUI](https://github.com/Daplanet/baseui/blob/master/README.md) and the Companions [BaseAPI](https://github.com/Daplanet/baseapi/blob/master/README.md) / [DataGrid](https://github.com/Daplanet/datagrid/blob/master/README.md).


## Roadmap
### Upcoming
* vue-meta support
* react-snap support
* ipfs support

## Usage

```
git clone --depth=1 https://github.com/Daplanet/baseui baseui && cd $_
$EDITOR src/app.js # add define() blocks for new VueJS views/controllers
make all
```

### Requirements
- Docker for OSx/Windowws or just a Linux machine with docker.
- GNU make
- VIM/Emacs/$EDITOR of choice
- ipfs, s3, cloudflair cdn, or datagrid/communitygrid (optional)
- Your app's Assets / copytext


## License
[![FOSSA Status](https://app.fossa.com/api/projects/git%2Bgithub.com%2Fdaplanet%2Fbaseui.svg?type=large)](https://app.fossa.com/projects/git%2Bgithub.com%daplanet%2Fbaseui?ref=badge_large)
